const express = require('express');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const path = require('path');
const bodyParser = require('body-parser');

const app = express();
const port = 8080;

var corsOptions = {
  origin: '*', method: 'GET, PUT, PATCH, POST', optionsSuccessStatus: 200
};

app.use(cors(corsOptions));
app.use(fileUpload());
app.use(bodyParser.json());

const filename = path.basename('/1.zip');

app.route('/test').get((req, res) => {
  res.setHeader('Content-Type', 'application/json');
  res.send([{ value: 'hello clover', 'User Name': 'clover' }]);
  // res.send({ value: 'hello clover', 'User Name': 'clover' });
  console.log(req.query);
})

app.route('/test').post((req, res) => {
  res.setHeader('Content-Type', 'application/json');
  res.send([{ value: 'hello clover', 'User Name': 'clover' }]);
  // res.send({ value: 'hello clover', 'User Name': 'clover' });
  console.log(req.body);
})

app.route('/upload').post((req, res) => {
  // res.setHeader('Content-Disposition', `attachment; filename=${filename}`);   // <- no working.
  res.setHeader('Content-Type', 'multipart/form-data');
  res.setHeader('Content-Transfer-Encoding', 'binary');
  console.log(req.files);
  res.send({ result: 'success', message: 'Success' });
})

app.post('/uploading', (req, res) => {
  console.log(req.files);
  console.log(req.body.settings);
  res.send({ result: 'success', message: 'Success' });
})

app.listen(port, () => console.log(`example app listening on port ${port}`));

